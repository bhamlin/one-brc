use std::{
    cmp,
    collections::HashMap,
    env,
    fmt::Display,
    fs,
    io::{BufRead, BufReader},
    rc::Rc,
    time::Instant,
};

fn main() {
    let args = env::args().collect::<Vec<_>>();
    if args.len() != 2 {
        println!("Usage: {} path/to/measurement.txt", args[0]);
    }

    let start_time = Instant::now();

    let measurement_file = fs::File::open(&args[1]).unwrap();
    let reader = BufReader::new(measurement_file);
    let mut data = HashMap::<String, StationData>::new();
    let mut count = 0;

    for line in reader.lines() {
        let line = line.unwrap();
        if let Some((name, temp_raw)) = line.split_once(';') {
            count += 1;
            let temp = parse_temp(temp_raw);
            data.entry(name.to_string())
                .and_modify(|e| {
                    e.min = cmp::min(e.min, temp);
                    e.max = cmp::max(e.max, temp);
                    e.sum += temp;
                    e.count += 1;
                })
                .or_insert(StationData::new(name, temp));
        }
    }

    let mut cities = data.keys().collect::<Vec<_>>();
    cities.sort();

    let duration = start_time.elapsed();

    // for city in cities {
    //     println!("{}", data.get(city).unwrap());
    // }

    println!("Processed {} rows in {:?}", count, duration);
}

fn parse_temp(input: &str) -> i64 {
    (input.parse::<f64>().unwrap() * 10.0).floor() as i64
}

struct StationData {
    name: Rc<str>,
    min: i64,
    max: i64,
    sum: i64,
    count: usize,
}

impl Display for StationData {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let min = self.min as f64 / 10.0;
        let max = self.max as f64 / 10.0;
        let avg = (self.sum / self.count as i64) as f64 / 10.0;

        write!(f, "{}: {:0.1}/{:0.1}/{:0.1}", self.name, min, avg, max)
    }
}

impl StationData {
    fn new(name: &str, temp: i64) -> Self {
        Self {
            name: Rc::from(name),
            min: temp,
            max: temp,
            sum: temp,
            count: 1,
        }
    }
}
