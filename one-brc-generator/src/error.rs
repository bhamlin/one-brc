use error_stack::Context;
use std::fmt::Display;

pub type EmptyResult = error_stack::Result<(), GeneratorError>;

#[derive(Debug)]
pub enum GeneratorError {
    FileNotFound,
    UnableToReadFile,
    FloatParseError,
}

impl Context for GeneratorError {}
impl Display for GeneratorError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::FileNotFound => "File not found",
            Self::UnableToReadFile => "Unable to open file",
            Self::FloatParseError => "Unable to parse float in file",
        };

        write!(f, "{}", message)
    }
}
