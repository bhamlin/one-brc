use error::{EmptyResult, GeneratorError};
use error_stack::ResultExt;
use indicatif::{ProgressBar, ProgressStyle};
use log::{debug, info};
use opts::Options;
use station::WeatherStations;
use std::{
    fs,
    io::{self, BufRead, BufReader, BufWriter, Write},
    sync::Arc,
};

mod error;
mod opts;
mod station;

fn main() -> EmptyResult {
    let opts = crate::opts::parse_args();

    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or(log::Level::Info.as_str()),
    )
    .init();

    debug!("{opts:?}");

    // Load city names and build generator map
    let mut stations = load_stations(&opts)?;

    // Buffered writer that targets either the file specified, or stdout
    let mut out_writer = BufWriter::new(match &opts.output_file {
        Some(path) => {
            let file = fs::File::create(path.to_string())
                .change_context_lazy(|| GeneratorError::FileNotFound)?;
            Box::new(file) as Box<dyn Write>
        }
        None => Box::new(io::stdout()) as Box<dyn Write>,
    });

    info!(
        "Generating {} lines to {}",
        opts.line_count,
        match opts.output_file {
            Some(path) => path,
            None => "stdout".into(),
        }
    );
    let pb = ProgressBar::new((opts.line_count * 15) as u64); // Average line size is 14~15 bytes
    pb.set_style(ProgressStyle::with_template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} ({bytes_per_sec}, {eta})")
        .unwrap()
        .progress_chars("#>-"));
    for _ in 1..=opts.line_count {
        let station = stations.pick_random_station();
        let temp = stations.generate_measurement(&station);
        let line = format!("{};{}", station, temp);
        writeln!(out_writer, "{line}").ok();
        pb.inc(line.len() as u64);
    }
    pb.finish();
    info!("Done");

    Ok(())
}

fn load_stations(opts: &Options) -> error_stack::Result<WeatherStations, GeneratorError> {
    let file = fs::File::open(opts.station_list_path.to_string())
        .change_context_lazy(|| GeneratorError::FileNotFound)
        .attach_printable_lazy(|| opts.station_list_path.clone())?;
    let reader = BufReader::new(file);

    let mut station_list = WeatherStations::new(opts.stdev);

    for line in reader.lines() {
        let line = line.change_context_lazy(|| GeneratorError::UnableToReadFile)?;
        if let Some((name, mean_temp)) = line.split_once(';') {
            let name_rc: Arc<str> = Arc::from(name);
            let mean = mean_temp
                .parse::<f64>()
                .change_context_lazy(|| GeneratorError::FloatParseError)?;
            station_list.add_station(name_rc, mean);
        }
    }

    station_list.lock_stations();
    Ok(station_list)
}
