use rand::RngCore;
use rand_distr::{Distribution, Normal};
use std::{collections::HashMap, sync::Arc};

pub struct WeatherStations {
    // Map of stations with a Normal distribution generator
    pub station_map: HashMap<Arc<str>, Normal<f64>>,

    // Vector cache of station names for quick random selection
    pub stations: Vec<Arc<str>>,

    // Default random source to one day allow for seeded generation
    pub random_source: Box<dyn RngCore>,

    // Standard deviation to use for all sites
    pub station_dev: f64,
}

impl WeatherStations {
    pub fn new(station_dev: f64) -> Self {
        Self::with_rng(rand::thread_rng(), station_dev)
    }

    pub fn with_rng(random_source: impl RngCore + Sized + 'static, station_dev: f64) -> Self {
        WeatherStations {
            station_map: Default::default(),
            stations: Default::default(),
            random_source: Box::new(random_source),
            station_dev,
        }
    }

    pub fn add_station(&mut self, name: Arc<str>, mean: f64) {
        let gaussian = Normal::new(mean, self.station_dev).unwrap();
        self.station_map.insert(name, gaussian);
    }

    pub fn lock_stations(&mut self) {
        self.stations.extend(self.station_map.keys().cloned());
    }

    pub fn pick_random_station(&mut self) -> Arc<str> {
        let station_list_size = self.station_map.len();
        let index = self.random_source.next_u32() as usize % station_list_size;
        self.stations.get(index).unwrap().clone()
    }

    pub fn generate_measurement(&mut self, station: &Arc<str>) -> f64 {
        let temp = self
            .station_map
            .get(station)
            .unwrap()
            .sample(&mut self.random_source);

        ceil(temp)
    }
}

#[inline]
/// IEEE 754 round to positive infinity emulation
fn ceil(input: f64) -> f64 {
    (input * 10.0).ceil() / 10.0
}
