use clap::Parser;
use std::sync::Arc;

#[derive(Debug, Parser)]
pub struct Options {
    /// File containing bare lines of UTF-8 encoded station names.
    pub station_list_path: Arc<str>,

    /// Quantity of lines to output.
    pub line_count: usize,

    #[clap(short, long)]
    /// File to write out to, else STDOUT.
    pub output_file: Option<Arc<str>>,

    #[clap(short, long, default_value_t = 10.0)]
    /// Standard deviation for randomness distribution
    pub stdev: f64,
}

/// Parse arguments and return Options struct
pub fn parse_args() -> Options {
    Options::parse()
}
